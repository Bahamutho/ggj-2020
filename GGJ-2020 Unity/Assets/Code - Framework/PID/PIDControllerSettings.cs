﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu]
[InlineEditor()]
public class PIDControllerSettings : ScriptableObject
{
    [Tooltip(
        "Constant that multiplies with the current error. Error being the difference between target and current whatever (position for example).")]
    public float ProportionalGain = 1f;

    [Tooltip("Based on past error changes")]
    public float IntegralGain = 0f;

    [Tooltip("Predicts future by change in error")]
    public float DerivativeGain = .1f;

    public float MaxForce = 100f;
}