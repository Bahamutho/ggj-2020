﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveRigidbody2DToTarget : MonoBehaviour
{
    public MoveToTargetPidSettings PidSettings;
    private PIDControllerSettings xSettings => PidSettings.XPidSettings;
    private PIDControllerSettings ySettings => PidSettings.YPidSettings;
    private PIDControllerSettings zSettings => PidSettings.ZPidSetttings;
    public Transform Target;
    [EnumFlag] public VectorAxes Axes;
    public bool On = true;
    private Rigidbody2D rb;

    [SerializeField] private PIDController pidControllerX, pidControllerY, pidControllerZ;
    private Vector3 targetPos => Target.position;
    private Vector3 currentPos => rb.position;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        pidControllerX = new PIDController(xSettings);
        pidControllerY = new PIDController(ySettings);
        pidControllerZ = new PIDController(zSettings);
    }

    void FixedUpdate()
    {
        if(!On) return;

        pidControllerX.constants = PidSettings.XPidSettings;
        pidControllerY.constants = PidSettings.YPidSettings;
        pidControllerZ.constants = PidSettings.ZPidSetttings;

        Vector3 error = targetPos - currentPos;

        float x = Axes.HasFlag(VectorAxes.X) ? pidControllerX.Update(error.x, Time.fixedDeltaTime) : 0;
        float y = Axes.HasFlag(VectorAxes.Y) ? pidControllerY.Update(error.y, Time.fixedDeltaTime) : 0;
        float z = Axes.HasFlag(VectorAxes.Z) ? pidControllerZ.Update(error.z, Time.fixedDeltaTime) : 0;
        float xForce = x * xSettings.MaxForce;
        float yForce = y * ySettings.MaxForce;
        float zForce = z * zSettings.MaxForce;
        rb.AddForce(new Vector3(xForce, yForce, zForce));
    }
}

public enum VectorAxes
{
    X = (1 << 0),
    Y = (1 << 1),
    Z = (1 << 2),
}