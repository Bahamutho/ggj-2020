﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LabelDrawer : MonoBehaviour
{
    public string Label = "Label";
    public Color SphereColor = Color.gray;
    [Range(0.01f,10f)]
    public float Radius = 0.25f;

    void OnDrawGizmos()
    {
#if UNITY_EDITOR
        Handles.Label(transform.position,Label);
        Gizmos.color = SphereColor;
        Gizmos.DrawWireSphere(transform.position,Radius);
#endif
    }
}
