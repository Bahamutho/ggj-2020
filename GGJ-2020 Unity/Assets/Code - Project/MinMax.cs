﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
[InlineProperty]
public class MinMax : ScriptableObject
{
    [SerializeField]
    private float min;
    [SerializeField]
    private float max;

    public float Max => Mathf.Max(min, max);
    public float Min => Mathf.Min(min, max);
}
