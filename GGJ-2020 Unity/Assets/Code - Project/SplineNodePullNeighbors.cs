﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.U2D;

[ExecuteInEditMode]
public class SplineNodePullNeighbors : MonoBehaviour
{
    public int IndexToMove = 0;
    public Vector2 Move = Vector2.down;

    private SplineState state;
    private List<SplinePoint> points => state.SplinePoints;
    
    void Start()
    {
        if (state == null) state = GetComponent<SplineState>();
    }

    [Button]
    public void MoveIndex()
    {
        state.SplinePoints[IndexToMove].Current += Move;
    }

    [Button]
    public void MoveNeighbors()
    {
        var pointCount = points.Count;
        points.ForEach(p =>
        {
            int i = p.Index;

            int nexti = SplineUtility.NextIndex(i, pointCount);
            int previ = SplineUtility.PreviousIndex(i, pointCount);
            SplinePoint prev = points[previ];
            SplinePoint next = points[nexti];
            SplinePoint current = points[i];

            points[previ].Velocity += CalculateVelocityAfterPull(current, prev);
            points[nexti].Velocity += CalculateVelocityAfterPull(current, next);
            points[i].Velocity += CalculateVelocityAfterPull(current.Anchor, current.Current, 0f);
        });
    }

    private Vector2 CalculateVelocityAfterPull(SplinePoint puller, SplinePoint point)
    {
        float anchorLength = (puller.Anchor - point.Anchor).magnitude;
        return CalculateVelocityAfterPull(puller.Current, point.Current, anchorLength);
    }

    private Vector2 CalculateVelocityAfterPull(Vector2 puller, Vector2 point, float restDistance)
    {
        Vector2 moveAlong = point - puller;
        Vector2 desiredNewPoint = puller + moveAlong.normalized * restDistance;
        return desiredNewPoint - point;
    }


}
