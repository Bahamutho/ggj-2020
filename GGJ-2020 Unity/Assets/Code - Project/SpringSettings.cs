﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpringSettings
{
    public float Damping;
    public float Stiffness;
}
