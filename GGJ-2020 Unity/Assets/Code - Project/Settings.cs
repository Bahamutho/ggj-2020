using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Settings : ScriptableObject
{
    [Header("Jelleh settings")]
    [Range(0, 1)]
    public float Stiffness = 0.3f;
    [Range(0, 1)]
    public float Damping = 0.3f;

    [Header("Impact Intensity Modifiers")]
    public float ImpactModifier = 10f;
    public float MinimumImpact = 10f;

    [Header("Radius Modifiers")]
    public float ImpactRadiusModifier = 0.3f;
    public float MinimumImpactRadius = 4f;

    [Header("Jump Settings")]
    public float TimeToMaxJumpApex = 1.0f;
    public float TimeToMinJumpApex = 0.5f;
    public float TimeToFall = 0.5f;
    public float MaxJumpHeight = 5.0f;
    public float MinJumpHeight = 1.0f;
    public float MinJumpSlowDownGravityScale = 35f;
    public float CoyoteTime = 0.3f;
    public bool EnableMinimumJump = false;
    public float WalkingGravity = 1.0f;

    [Header("Player Spring and IK Settings")]

    public SpringSettings FootToTargetSpringSettings;
    public LayerMask IkColliderLayerMask;
    public float StepRadius = 0.3f;
    public int CalculatePreSteps = 4;
    [Range(4, 20)]
    public int RaysPerCircle = 8;
    [Range(-1f, 1f)]
    public float maxAllowedDotNormalAngleForFeet = 0.2f;
    public int MinimumCollidingForGrounded = 2;
    public int MinimumCollidingForNotGrounded = 0;

    [Header("Other settings")]
    public float BackgroundMoveSpeed = 1.0f;
    public float TitleFadeOutTime = 1.65f;
    public float TitleVisible = 3.0f;
    public float WaitForNextStateTime = 20.0f;

    [Header("Particles")]
    public Color YinParticleColor = Color.blue;
    public Color YangParticleColor = Color.white;
    public float ParticleTimeToHeart = 1.0f;

    [Header("Particle Spawning")]
    public float FastSpawnPerSecond = 3;
    public float NormalSpawnPerSecond = 0.5f;
    public float YangImpactTreshold = 10f;
    public float YangExplosionTimePerParticle = 0.1f;
    public float DisabledYangParticleTimeOnSpawn = 0.5f;
    public float InitialYangParticleSpeed = 3f;
    public float InitialYinParticleSpeed = 2.0f;

    [Header("Plant Growth")]
    public float GrowthPerCollision = 0.1f;
    public float StartPlantGrowth = 0.01f;
    public float MaxPlantSize = 1.25f;

    [Header("Color of states")]
    public Color NightColor = Color.white;
    public Color DawnColor = Color.white;
    public Color DayColor = Color.white;
    public float StateChangeTime = 3.0f;

    public float ImpactRadius(float impact)
    {
        return Mathf.Max(ImpactRadiusModifier * impact, MinimumImpactRadius);
    }

    public float Impact(float impact)
    {
        return Mathf.Max(ImpactModifier * impact, MinimumImpact);
    }

    public float SpawnPerSecond(ParticleSpawnRate rate)
    {
        switch (rate)
        {
            case ParticleSpawnRate.Fast:
                return FastSpawnPerSecond;
            case ParticleSpawnRate.Normal:
                return NormalSpawnPerSecond;
            default:
                return 0;
        }
    }

    public Color ColorForState(GameState state)
    {
        switch (state)
        {
            case GameState.Night:
                return NightColor;
            case GameState.Dawn:
                return DawnColor;
            default:
                return DayColor;
        }
    }

    
}