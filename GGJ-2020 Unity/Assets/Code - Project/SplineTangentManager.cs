﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.U2D;

[ExecuteInEditMode]
public class SplineTangentManager : MonoBehaviour
{
    [Range(0.1f,1.5f)]
    public float TangentPercentage = 0.3f;
    private SpriteShapeController _shape;
    private Spline Spline => _shape.spline;
    private SplineState state;

    private void Start()
    {
        if (_shape == null) _shape = GetComponent<SpriteShapeController>();
    }

    [Button]
    public void SetTangents()
    {
        var pointCount = Spline.GetPointCount();
        for (int i = 1; i < pointCount; i++)
        {
            Spline.SetTangentMode(i, ShapeTangentMode.Continuous);

            Vector2 prev = Spline.GetPosition(SplineUtility.PreviousIndex(i, pointCount));
            Vector2 next = Spline.GetPosition(SplineUtility.NextIndex(i, pointCount));
            Vector2 current = Spline.GetPosition(i);
            Vector2 angle = (next - prev).normalized;
            float distRight = Vector2.Dot(next - current, angle);
            float distLeft = Vector2.Dot(prev - current, angle);

            Vector3 leftTangent = angle * distLeft * TangentPercentage;
            Vector2 rightTangent = angle * distRight * TangentPercentage;

            Spline.SetLeftTangent(i, leftTangent);
            Spline.SetRightTangent(i, rightTangent);
        }
    }
}