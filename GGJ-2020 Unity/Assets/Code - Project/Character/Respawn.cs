﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    private Rigidbody2D rb;
    public Transform SpawnPosition;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    [Button]
    public void RespawnPlayer()
    {
        rb.position = SpawnPosition.position;
        rb.velocity = Vector2.zero;
        rb.inertia = 0f;
    }
}
