using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float TimeToMaxSpeed = 1.0f;
    public float TimeToDeAccel = 0.5f;
    public float MaxSpeed = 5.0f;

    private Rigidbody2D rb;
    private float horInput = 0f;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Discrete steps 1 / 120 
    private void FixedUpdate()
    {
        HandleMoveX();
    }

    public void SetInput(float input)
    {
        horInput = input;
    }

    private void HandleMoveX()
    {
        float vx = rb.velocity.x;
        float xVelocity = 0.0f;
        bool accelerating = System.Math.Abs(horInput - Mathf.Sign(vx)) < 0.01f;
        bool stopped = System.Math.Abs(vx) < 0.1f;
        bool noInput = System.Math.Abs(horInput) < 0.01f;
        if (accelerating)
        {
            float accel = MaxSpeed * (Time.fixedDeltaTime / TimeToMaxSpeed);
            xVelocity = Mathf.Clamp(vx + horInput * accel, -MaxSpeed, MaxSpeed);
        }
        else if (stopped && noInput)
        {
            xVelocity = 0;
        }
        else //breaking
        {
            float deAccel = Mathf.Sign(vx) * MaxSpeed * (Time.fixedDeltaTime / TimeToDeAccel);
            xVelocity = vx - deAccel;
        }
        rb.velocity = new Vector2(xVelocity, rb.velocity.y);
    }
}
