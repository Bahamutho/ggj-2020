﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class JumpController : MonoBehaviour
{
    public Settings Settings;

    private Rigidbody2D rb;
    private CharacterMovementState state;
    private float lastGrounded;

    public float LaunchHeight = -1f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        state = GetComponent<CharacterMovementState>();
    }

    void FixedUpdate()
    {
        if (state.Grounded)
        {
            if (!state.PreviousGrounded)  // Wasn't grounded before
                rb.gravityScale = Settings.WalkingGravity;

            lastGrounded = Time.timeSinceLevelLoad;
        }

        if (rb.velocity.y < -0.01f && !state.Grounded)
        {
            state.Falling = true;

            float gravity = 2 * Settings.MaxJumpHeight / (Settings.TimeToFall * Settings.TimeToFall);
            rb.gravityScale = GravityScale(gravity);
        }
        else
            state.Falling = false;
    }

    public void StartJump()
    {
        float now = Time.timeSinceLevelLoad;
        bool outsideCoyoteTime = now > lastGrounded + Settings.CoyoteTime;
        if (!state.Grounded && outsideCoyoteTime)
            return;

        float gravity = 2 * Settings.MaxJumpHeight / (Settings.TimeToMaxJumpApex * Settings.TimeToMaxJumpApex);
        rb.gravityScale = GravityScale(gravity);

        float velocity = gravity * Settings.TimeToMaxJumpApex;
        rb.velocity = new Vector2(rb.velocity.x, velocity);

        Events.Instance.Raise(new OnJumpStart());
    }

    private float GravityScale(float gravity)
    {
        return -gravity / Physics2D.gravity.y;
    }


    public void EndJump()
    {
        if (!Settings.EnableMinimumJump)
            return;

        bool alreadyFalling = rb.velocity.y <= 0;
        if (alreadyFalling)
            return;

        bool higherThanMinJumpHeight = rb.velocity.y > LaunchHeight + Settings.MinJumpHeight;
        if (higherThanMinJumpHeight)
        {
            rb.gravityScale = Settings.MinJumpSlowDownGravityScale;
        }
        else
        {
            // Did not reach minApex yet
            float timeToReachApex = Settings.TimeToMinJumpApex;
            var gravity = rb.velocity.y / timeToReachApex;
            rb.gravityScale = GravityScale(gravity);
        }
    }
}

public class OnJumpStart : GameEvent
{

}