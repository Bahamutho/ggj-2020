﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterMovementState))]
public class PlayerAnimationVariables : MonoBehaviour
{
    private CharacterMovementState state;
    private Animator animator;

    private int grounded, jump, falling;

    void Start()
    {
        state = GetComponent<CharacterMovementState>();
        animator = GetComponent<Animator>();
        grounded = Animator.StringToHash("Grounded");
        jump = Animator.StringToHash("Jumping");
        falling = Animator.StringToHash("Falling");
    }

    // Update is called once per frame
    void LateUpdate()
    {
        animator.SetBool(grounded, state.Grounded);
        animator.SetBool(jump, state.Jumping);
        animator.SetBool(falling, state.Falling);
    }
}
