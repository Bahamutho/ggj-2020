﻿using System;
using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;

[ExecuteInEditMode]
public class FootTargetMover : MonoBehaviour
{
    public Transform FootTarget;
    public FootLocationPredictor FootPredictor;
    public Transform FootNeutral;
    public Rigidbody2D Torso;

    public MinMax LeftMinMax;
    public MinMax RightMinMax;

    public bool On = true;

    [Header("Debug")] public float TotalProgress;
    public float RemappedTotalProgress;
    public double angleProgress;
    public double goalAngle;
    public double progress;
    public int currentIndex;
    public float departureAngle;

    private const float TwoPi = Mathf.PI * 2;

    private Settings settings => SettingsHolder.Instance.Settings;

    [Button]
    void Update()
    {
        if (!On) return;

        TotalProgress = CalculateCurrentProgress(out var isLeft, out float xProgress);
        FootTarget.position = CalculateTarget(FootPredictor, isLeft, TotalProgress);
        FootPredictor.UpdateTargetPos(TotalProgress, xProgress);
    }

    private float CalculateCurrentProgress(out bool isLeft, out float xProgress)
    {
        xProgress = CalculateXProgress();
        isLeft = xProgress < 0;
        return FootPredictor.CalculateFootProgress(xProgress);
    }

    private float CalculateXProgress()
    {
        Vector2 neutralFootOffset = (Vector2) FootNeutral.position - Torso.position;
        Vector2 OriginalTorsoPosition = (Vector2) FootPredictor.transform.position - neutralFootOffset;

        float xProgress = (Torso.position - OriginalTorsoPosition).x;
        return xProgress;
    }

    private Vector2 CalculateTarget(FootLocationPredictor predictor, bool isLeft, float totalProgress)
    {
        totalProgress = Mathf.Abs(totalProgress);
        float radius = predictor.Radius;

        List<Vector2> feetPositions = isLeft ? predictor.BackwardFeetPosition : predictor.ForwardFeetPosition;
        currentIndex = CalculateIndex(totalProgress, feetPositions);

        var center = CalculateCenter(isLeft, feetPositions, radius);

        angleProgress = CalculateAngleProgress(isLeft, totalProgress, feetPositions, center);

        return CircleMath.PosOnParametricCircle(center, radius, (float) angleProgress);
    }

    private double CalculateAngleProgress(bool isLeft, float totalProgress, List<Vector2> feetPositions, Vector2 center)
    {
        RemappedTotalProgress = CalculateAndRemapProgress(totalProgress, feetPositions, isLeft);
        departureAngle = isLeft ? 0f : (float) Math.PI;
        goalAngle = CalculateGoalAngle(center, feetPositions, currentIndex);

        var angleProgress = departureAngle + (goalAngle - departureAngle) * RemappedTotalProgress;
        return angleProgress % (TwoPi);
    }

    public bool isLeft;

    private float CalculateAndRemapProgress(float totalProgress, List<Vector2> feetPositions, bool isLeft)
    {
        var progress = totalProgress >= feetPositions.Count - 1 ? 1 : totalProgress - currentIndex;
        this.progress = progress;
        this.isLeft = isLeft;
        MinMax current = isLeft ? LeftMinMax : RightMinMax;
        return RemapProgress(progress, current);
    }

    public float min, max;

    private float RemapProgress(float progress, MinMax minMax)
    {
        min = minMax.Min;
        max = minMax.Max;
        return Mathf.Clamp01((progress - min) / (max - min));
    }

    private Vector2 CalculateCenter(bool isLeft, List<Vector2> feetPositions, float radius)
    {
        Vector2 departure = feetPositions[currentIndex];
        return departure + (isLeft ? Vector2.left : Vector2.right) * radius;
    }

    private static float CalculateGoalAngle(Vector2 center, List<Vector2> feetPositions, int currentIndex)
    {
        Vector2 goal = feetPositions[currentIndex + 1];
        Vector2 dirVector = goal - center;
        float angle = Mathf.Atan2(dirVector.y, dirVector.x);
        angle = (TwoPi + angle) % TwoPi;
        bool closeToTwoPi = Mathf.Abs(angle - TwoPi) <= 0.1f;
        return closeToTwoPi ? 0 : angle;
    }

    private int CalculateIndex(float totalProgress, List<Vector2> feetPositions)
    {
        int maxStartIndex = feetPositions.Count - 2;
        return Math.Min(Mathf.FloorToInt(totalProgress), maxStartIndex);
    }
}