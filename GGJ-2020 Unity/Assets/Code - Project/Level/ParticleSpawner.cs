﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    public ParticleSpawnRate Rate;
    public Settings Settings;

    [SerializeField]
    private float progressOfNewParticlesToSpawn = 0;

    // Update is called once per frame
    void Update()
    {
        progressOfNewParticlesToSpawn += Settings.FastSpawnPerSecond * Time.deltaTime;
        SpawnParticles();
    }

    private void SpawnParticles()
    {
        var particlesToSpawn = progressOfNewParticlesToSpawn > 0 ? Mathf.FloorToInt(progressOfNewParticlesToSpawn) : 0;
        
        for (var i = 0; i < particlesToSpawn; i++)
        {
            var spawn = ObjectPoolCollection.Instance.ParticlesPool.Spawn(transform.position);
            var particleEntity = spawn.GetComponent<ParticleEntity>();
            particleEntity.LaunchYin();
        }

        progressOfNewParticlesToSpawn -= particlesToSpawn;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
#if UNITY_EDITOR
        Handles.Label(transform.position, $"PS: [{Rate}]");
#endif
        Gizmos.DrawSphere(transform.position, 0.05f);
    }
}

public enum ParticleSpawnRate
{
    Normal,
    Fast,
    Off
}
