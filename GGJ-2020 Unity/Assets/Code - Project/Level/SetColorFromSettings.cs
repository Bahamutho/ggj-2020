﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class SetColorFromSettings : MonoBehaviour
{
    private SpriteRenderer renderer;
    private Settings settings;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();
        settings = SettingsHolder.Instance.Settings;
        Events.Instance.AddListener<GameStateTransition>(OnStateChange);
    }

    private void OnStateChange(GameStateTransition e)
    {
        Color startColor = renderer.color;
        Color targetColor = settings.ColorForState(e.TransitionToState);
        // Debug.Log($"Change {e.TransitionToState} from {startColor} to {targetColor} in {settings.StateChangeTime} seconds");
        if (e.TransitionToState == GameState.Night)
        {
            renderer.color = targetColor;
            return;
        }

        StartCoroutine(CoroutineLibrary.LerpProgressCoroutine(
            settings.StateChangeTime,
            progress => renderer.color = Color.Lerp(startColor, targetColor, progress)));
    }
}