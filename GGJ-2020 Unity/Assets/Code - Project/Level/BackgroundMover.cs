﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class BackgroundMover : MonoBehaviour
{
    public Settings Settings;
    public SpriteRenderer Title;

    public float NightHeight = -28.1f;
    public float DawnHeight = 5.5f;
    public float DayHeight = 34.9f;

    [SerializeField]
    private GameState movingTowards = GameState.Night;

    private bool titleVisible = true;

    // Start is called before the first frame update
    void Start()
    {
        Events.Instance.AddListener<GameStateTransition>(GoToState);
    }

    private void GoToState(GameStateTransition e)
    {
        movingTowards = e.TransitionToState;
    }

    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        var target = NextHeight();
        if (Mathf.Abs(pos.y - target) >= 0.5f)
        {
            var sign = Mathf.Sign(target - pos.y);
            pos.y += sign * Settings.BackgroundMoveSpeed * Time.deltaTime;
            transform.position = pos;
        }
        else if(movingTowards == GameState.Dawn && titleVisible)
        {
            titleVisible = false;
            StartCoroutine(FadeOutTitle());
        }
    }

    private IEnumerator FadeOutTitle()
    {
        yield return new WaitForSeconds(Settings.TitleVisible);

        float startTime = Time.timeSinceLevelLoad;
        float endTime = startTime + Settings.TitleFadeOutTime;
        float now = startTime;
        while (endTime > now)
        {
            now = Time.timeSinceLevelLoad;
            float cur = now - startTime;
            float progress = cur / (endTime - startTime);
            var titleColor = Title.color;
            titleColor.a = 1 - progress;
            Title.color = titleColor;
            yield return null;
        }
    }

    private float NextHeight()
    {
        switch (movingTowards)
        {
            case GameState.Dawn:
                return DawnHeight;
            case GameState.Day:
                return DayHeight;
            default:
                return NightHeight;
        }
    }
}
