﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioCollection : ScriptableObject
{
    public AudioPerTransitionState night, dawn, day;
    public int currentIndex = 0;
    private GameState previousGamestate = GameState.Night;

    public AudioClip GetNextClip(GameState current, bool firstTime = false)
    {
        AudioPerTransitionState audioClips = GetAudio(current);

        if (!firstTime && previousGamestate == current)
        {
            currentIndex = Mathf.Min(currentIndex + 1, audioClips.AudioClips.Count - 1);
        }
        else
        {
            currentIndex = 0;
        }

        previousGamestate = current;

        return audioClips.AudioClips[currentIndex];
    }

    private AudioPerTransitionState GetAudio(GameState current)
    {
        switch (current)
        {
            case GameState.Dawn:
                return dawn;
            case GameState.Night:
                return night;
            default:
                return day;
        }
    }

    [System.Serializable]
    public class AudioPerTransitionState
    {
        public List<AudioClip> AudioClips;
    }
}
