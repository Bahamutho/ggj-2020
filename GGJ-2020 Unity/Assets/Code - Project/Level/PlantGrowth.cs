﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlantGrowth : MonoBehaviour
{
    public Settings Settings;

    [Range(0.01f,3f)]
    [SerializeField]
    private float growth = 1.0f;
    public float Growth
    {
        set => growth = Mathf.Clamp(value,0.01f, Settings.MaxPlantSize);
        get => growth;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(growth, growth, growth);
    }
}
