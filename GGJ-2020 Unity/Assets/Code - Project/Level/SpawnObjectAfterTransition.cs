﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class SpawnObjectAfterTransition : MonoBehaviour
{
    public GameObject Prefab;

    public GameState SpawnState;
    [Range(0f,240f)]
    public float WaitForSpawn = 1.0f;

    public Vector2 Direction = Vector2.right;
    public float SpawnSpeed = 4f;


    void Start()
    {
        Events.Instance.AddListener<GameStateTransition>(SpecialEvents);
    }

    private void SpecialEvents(GameStateTransition e)
    {
        if (SpawnState == e.TransitionToState)
        {
            StartCoroutine(SpawnAfter());
        }
    }

    private IEnumerator SpawnAfter()
    {
        yield return new WaitForSeconds(WaitForSpawn);
        SpawnObject();
    }

    private void SpawnObject()
    {
        var head = GameObject.Instantiate(Prefab, transform.position, Quaternion.identity);
        head.GetComponent<Rigidbody2D>().velocity = Direction.normalized * SpawnSpeed;
    }
}
