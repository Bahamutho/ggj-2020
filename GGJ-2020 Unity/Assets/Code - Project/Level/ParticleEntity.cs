﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEntity : MonoBehaviour
{
    public ParticleType Type;
    public Settings Settings;

    private SpriteRenderer renderer;
    private Rigidbody2D rb;
    private int _noCollisionLayer;
    private int _particleLayer;

    void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        _noCollisionLayer = LayerMask.NameToLayer("NoCollision");
        _particleLayer = LayerMask.NameToLayer("Particles");
        OnEnable();
    }

    void OnEnable()
    {
        SetColor();
        rb.simulated = true;
    }

    public void MoveToHeart(Transform heart, Action actionOnCompleteTravel)
    {
        rb.simulated = false;
        StartCoroutine(TravelToHeart(heart, actionOnCompleteTravel));
    }

    private IEnumerator TravelToHeart(Transform heart, Action action)
    {
        Vector2 pos = transform.position;
        float timeStart = Time.timeSinceLevelLoad;
        float timeEnd = timeStart + Settings.ParticleTimeToHeart;
        float now = timeStart;
        float progress = 0;

        while (progress < 1.0f)
        {
            now = Time.timeSinceLevelLoad;
            progress = (now - timeStart) / (timeEnd - timeStart);
            transform.position = Vector3.Lerp(pos, heart.position, progress);
            yield return null;
        }

        gameObject.SetActive(false);
        action.Invoke();
    }

    public void SetYangExplosion()
    {
        Type = ParticleType.Yang;
        SetColor();
        StartCoroutine(TravelFromHeart());
    }

    private IEnumerator TravelFromHeart()
    {
        rb.simulated = true;
        Vector2 randomDir = new Vector2(Random(), Random()).normalized;
        rb.velocity = randomDir * Settings.InitialYangParticleSpeed * UnityEngine.Random.Range(-0.8f, 1.2f);

        gameObject.layer = _noCollisionLayer;
        yield return new WaitForSeconds(Settings.DisabledYangParticleTimeOnSpawn);
        gameObject.layer = _particleLayer;
    }

    private void SetColor()
    {
        Color c = (this.Type == ParticleType.Yin) ? Settings.YinParticleColor : Settings.YangParticleColor;
        renderer.color = c;
    }

    public void LaunchYin()
    {
        Type = ParticleType.Yin;
        SetColor();
        Vector2 randomUp = new Vector2(Random(), UnityEngine.Random.Range(0.001f, 1f)).normalized;
        rb.velocity = randomUp * Settings.InitialYinParticleSpeed * UnityEngine.Random.Range(-0.8f, 1.2f);
    }

    private float Random()
    {
        var sign = UnityEngine.Random.value > 0.5f ? 1 : -1;
        return sign * UnityEngine.Random.Range(0.001f, 1f);
    }
}

public enum ParticleType
{
    Yin,
    Yang
}