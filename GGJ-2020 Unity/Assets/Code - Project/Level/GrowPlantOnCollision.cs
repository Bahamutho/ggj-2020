﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GrowPlantOnCollision : MonoBehaviour
{
    public Settings Settings;
    public List<PlantGrowth> plantsOnNode = new List<PlantGrowth>();
    private string _particleTag = "Particle";
    private NodeAttach node;

    // Start is called before the first frame update
    void Start()
    {
        node = GetComponent<NodeAttach>();

        var nodes = GameObject.FindObjectsOfType<NodeAttach>();

        plantsOnNode = nodes
            .Where(n => n.index == node.index && n.GetComponent<PlantGrowth>() != null)
            .Select(n => n.GetComponent<PlantGrowth>())
            .ToList();

        foreach (var plantGrowth in plantsOnNode)
        {
            plantGrowth.Growth = Settings.StartPlantGrowth;
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag(_particleTag))
        {
            var entity = col.gameObject.GetComponent<ParticleEntity>();
            if (entity.Type == ParticleType.Yang)
            {
                if (plantsOnNode.Count > 0)
                    entity.MoveToHeart(transform, () =>
                    {
                        foreach (var plantGrowth in plantsOnNode)
                        {
                            plantGrowth.Growth += Settings.GrowthPerCollision;
                        }
                    });
            }
        }
    }
}