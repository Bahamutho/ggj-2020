﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class AudioTransitionManager : MonoBehaviour
{
    public AudioCollection AudioCollection;
    private AudioSource audiosource;

    private bool firstClip = true;
     
    // Start is called before the first frame update
    void Start()
    {
        audiosource = GetComponent<AudioSource>();
        Events.Instance.AddListener<ReadyForTransition>(HandleReadyForNextState);
        Events.Instance.AddListener<GameStateTransition>(ChangeTrack);
    }

    private void HandleReadyForNextState(ReadyForTransition e)
    {
        StopAllCoroutines();

        if (firstClip)
        {
            Debug.Log($"First transition {e.TransitionToState}");
            Events.Instance.Raise(new GameStateTransition(e.TransitionToState));
            return;
        }

        StartCoroutine(WaitForEndOfTrack(
            () => Events.Instance.Raise(new GameStateTransition(e.TransitionToState))));
    }

    private void ChangeTrack(GameStateTransition e)
    {
        PlayNextClip(e.TransitionToState);
        StartCoroutine(GoToNextClipWhenFinished(e.TransitionToState));
    }

    private void PlayNextClip(GameState state)
    {
        audiosource.clip = AudioCollection.GetNextClip(state, firstClip);
        audiosource.loop = true;
        audiosource.Play();
        firstClip = false;
        Debug.Log($"Playing {audiosource.clip.name}");
    }

    private IEnumerator GoToNextClipWhenFinished(GameState state)
    {
        while (true)
        {
            yield return WaitForEndOfTrack(() => PlayNextClip(state));
        }
    }

    private IEnumerator WaitForEndOfTrack(Action function)
    {
        yield return new WaitUntil(() =>
        {
            // Debug.Log($"{audiosource.clip.name} {audiosource.time}/{audiosource.clip.length}");
            return Mathf.Abs(audiosource.time - audiosource.clip.length) < 0.15f;
        });

        function.Invoke();
    }
}
