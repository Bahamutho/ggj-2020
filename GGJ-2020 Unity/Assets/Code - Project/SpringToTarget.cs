using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpringToTarget : MonoBehaviour
{
    public bool On = true;
    public Transform Target;
    [Range(0.1f, 5.0f)] public float Stiffness = 0.3f;
    [Range(0.1f, 1.0f)] public float Damping = 0.3f;
    [Range(-1f, 2.0f)] public float YOffset = 0.2f;
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
#if UNITY_EDITOR
    void Update()
    {
        if (!On)
            return;
        if (!Application.isPlaying)
        {
            transform.position = Target.position;
            return;
        }
        else
        {
            rb.MovePosition(Target.position);
        }
    }
#endif
    // void FixedUpdate()
    // {
    //     if(!On)
    //         return;
    //
    //
    //     Vector2 velocity = rb.velocity;
    //     Vector2 displacement = Target.position + Vector3.up * YOffset - transform.position;
    //     Vector2 impulse = Stiffness * displacement - Damping * velocity;
    //     rb.velocity += impulse;
    // }

    void OnDrawGizmos()
    {
        if(Target == null) return;
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, Target.position);
        Gizmos.DrawCube(Target.position, Vector3.one * 0.1f);
    }
}