﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedFoot : MonoBehaviour
{
    public bool Grounded;

    private Settings settings;
    private Collider2D col;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<Collider2D>();
        settings = SettingsHolder.Instance.Settings;
    }

    // Update is called once per frame
    void Update()
    {
        if(settings == null) settings = SettingsHolder.Instance.Settings;
        Grounded = col.IsTouchingLayers(settings.IkColliderLayerMask);
        // var hasGroundedPos = HasGroundedPos(out var groundedPos);
    }

    private bool HasGroundedPos(out Vector2 groundedPos)
    {
        bool hasGroundedPos = Grounded;
        groundedPos = transform.position;
        if (!Grounded)
        {
            var hit = Physics2D.Raycast(transform.position, Vector2.down, distance: 3f, layerMask: settings.IkColliderLayerMask);
            if (hit.collider != null)
            {
                hasGroundedPos = true;
                groundedPos = hit.point;
            }
        }

        return hasGroundedPos;
    }

    void OnDrawGizmos()
    {
        // if (!Grounded)
        // {
        //     Gizmos.color = Color.green;
        //     Gizmos.DrawWireCube(groundedPos, Vector3.one * 0.1f);
        // }
    }
}
