﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JellyClicker : MonoBehaviour
{
    [Header("CollisionImpact should move")]
    public Vector2 InfluencePoint = new Vector2(0, 1);
    public float Radius = 1f;
    public bool Clicked = false;
    public float CollisionForce = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        InfluencePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition); //Mouse.current.position.ReadValue();
        Clicked = Input.GetMouseButton(0);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Clicked ? Color.green : Color.red;
        Gizmos.DrawWireSphere(InfluencePoint, Radius);
    }
}
