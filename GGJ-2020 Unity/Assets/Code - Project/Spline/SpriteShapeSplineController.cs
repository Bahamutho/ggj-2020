﻿using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.U2D;

public class SpriteShapeSplineController : MonoBehaviour
{
    public Vector2 topLeft, topRight;
    public Vector2 bottomLeft, bottomRight;
    public int resolution = 10;
    public float waveOffset = 1f;

    private SpriteShapeController shape;
    private Spline spline => shape.spline;

    void Start()
    {
        this.shape = GetComponent<SpriteShapeController>();
    }

    [Button]
    void CreateSquare()
    {
        Start();

        spline.Clear();
        spline.InsertPointAt(0, topLeft);
        spline.InsertPointAt(1, topRight);
        spline.InsertPointAt(2, bottomRight);
        spline.InsertPointAt(3, bottomLeft);
        spline.isOpenEnded = false;
    }

    [Button]
    void CreateWave()
    {
        CreateSquare();
        SetupInBetweenPoints();
        for (int i = 1; i < resolution; i++)
        {
            spline.SetTangentMode(i, ShapeTangentMode.Continuous);
            
            Vector3 leftTangent, rightTangent;
            SplineUtility.CalculateTangents(
                spline.GetPosition(i),
                spline.GetPosition(SplineUtility.PreviousIndex(i,spline.GetPointCount())),
                spline.GetPosition(SplineUtility.NextIndex(i, spline.GetPointCount())),
                Vector3.up, 
                1f,
                out rightTangent,
                out leftTangent
                );
            spline.SetLeftTangent(i, leftTangent);
            spline.SetRightTangent(i, rightTangent);
        }
        //
        for (int i = 1; i < resolution; i++)
        {
            int direction = (i % 2) == 0 ? -1 : 1;
            Vector2 newPos = spline.GetPosition(i) + new Vector3(0, waveOffset * direction);
            spline.SetPosition(i, newPos);
        }
    }

    private void SetupInBetweenPoints()
    {
        for (int i = 1; i < resolution; i++)
        {
            var fraction = (topRight - topLeft) * ((float) i / resolution);
            Vector2 pos = topLeft + fraction;

            spline.InsertPointAt(i, pos);
        }
    }

}