using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class JellyImpactSystemManagerResource : MonoBehaviour
{
    public Settings Settings;
    private SplineState state;

    private void Start()
    {
        state = GetComponent<SplineState>();
        Events.Instance.AddListener<SlamJelly>(OnJellyImpact);
    }

    private void OnJellyImpact(SlamJelly e)
    {
        state.SplinePoints.ForEach(p =>
        {
            if (IsIntersecting(e.ImpactPos, Settings.ImpactRadius(e.Impact), p.Current + (Vector2) transform.position))
            {
                Vector2 ab = p.Current - e.ImpactPos;

                p.Velocity += ab.normalized * Settings.Impact(e.Impact) * Time.fixedDeltaTime;
                // Debug.Log(
                //     $"{p.Index} is colliding {p.Current} {e.ImpactPos} {ab} {ab.normalized} {e.Impact}");
            }
        });

    }
    private bool IsIntersecting(Vector2 p1, float r1, Vector2 p2)
    {
        return (p2 - p1).magnitude < r1;
    }
}
