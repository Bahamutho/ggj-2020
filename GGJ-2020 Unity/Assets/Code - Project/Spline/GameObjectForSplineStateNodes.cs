﻿using EasyButtons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

/// <summary>
/// Assumes to be on the spline class
/// </summary>
[ExecuteInEditMode]
public class GameObjectForSplineStateNodes : MonoBehaviour
{
    public bool SplineFollowsGameObjects = false;
    private SplineState state;
    [SerializeField]
    private List<SplineNodeGameObject> goSplineNodes = new List<SplineNodeGameObject>();

    // Start is called before the first frame update
    void Start()
    {
        SyncNodesAndCreateObjectsIfNeeded();
    }

    [Button]
    private void SyncNodesAndCreateObjectsIfNeeded()
    {
        if(state ==  null) state = GetComponent<SplineState>();

        if (goSplineNodes.Count != state.SplinePoints.Count)
        {
            goSplineNodes.ForEach(go => go.go.SetActive(false));
            goSplineNodes.Clear();
            SpriteShapeController ctx = GetComponent<SpriteShapeController>();

            foreach (var point in state.SplinePoints)
            {
                var spawn = ObjectPoolCollection.Instance.SplineNodePool.Spawn((Vector3)point.Current + transform.position);
                
                spawn.GetComponent<LabelDrawer>().Label = $"[{point.Index}]";
                var nodeAttach = spawn.GetComponent<NodeAttach>();
                nodeAttach.index = point.Index;
                nodeAttach.spriteShapeController = ctx;

                goSplineNodes.Add(new SplineNodeGameObject(spawn, point.Index, nodeAttach));
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        SyncNodesAndCreateObjectsIfNeeded();

        if(!SplineFollowsGameObjects) return;
        
        foreach (var node in goSplineNodes)
        {
            if (SplineFollowsGameObjects)
            {
                var pos = node.go.transform.localPosition - transform.position;
                state.SplinePoints[node.Index].Current = pos;
                state.SplinePoints[node.Index].Anchor = pos;
                node.NodeAttach.enabled = true;
            }
            else
                node.NodeAttach.enabled = false;
        }
    }
}

[System.Serializable]
public class SplineNodeGameObject
{
    public readonly int Index;
    public readonly NodeAttach NodeAttach;
    public readonly GameObject go;

    public SplineNodeGameObject(GameObject go, int index, NodeAttach nodeAttach)
    {
        this.go = go;
        Index = index;
        NodeAttach = nodeAttach;
    }
}
