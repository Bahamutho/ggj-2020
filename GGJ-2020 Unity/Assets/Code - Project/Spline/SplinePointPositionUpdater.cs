﻿using System.Collections.Generic;
using EasyButtons;
using UnityEngine;
using UnityEngine.U2D;

public class SplinePointPositionUpdater : MonoBehaviour
{
    public Settings Settings;

    public float MaxVelocity = 5.0f;
    public bool Spring = true;

    private SpriteShapeController _shape;
    private Spline Spline => _shape.spline;
    private SplineState state;
    private List<SplinePoint> points => state.SplinePoints;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        if (_shape == null) _shape = GetComponent<SpriteShapeController>();
        if (state == null) state = GetComponent<SplineState>();
    }

    void FixedUpdate()
    {
        state.SplinePoints.ForEach(p =>
        {
            if(Spring)
                p.UpdateVelocityWithSpring(Time.fixedDeltaTime, Settings.Damping, Settings.Stiffness);

            p.UpdatePosition(Time.fixedDeltaTime, MaxVelocity);
            var notAtTheSamePlace = ((Vector2) Spline.GetPosition(p.Index) - p.Current).magnitude > float.Epsilon;
            if (notAtTheSamePlace)
                Spline.SetPosition(p.Index, p.Current);
        });
    }

    [Button]
    public void UpdatePositionsBasedOnVelocity()
    {
        Init();
        points.ForEach(p =>
        {
            p.UpdatePosition(Time.fixedDeltaTime, MaxVelocity);
            var notAtTheSamePlace = ((Vector2)Spline.GetPosition(p.Index) - p.Current).magnitude > float.Epsilon;
            if (notAtTheSamePlace)
                Spline.SetPosition(p.Index, p.Current);
        });
    }
}