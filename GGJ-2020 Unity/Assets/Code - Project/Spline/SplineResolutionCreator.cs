﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using EasyButtons;
using UnityEngine;
using UnityEngine.U2D;

public class SplineResolutionCreator : MonoBehaviour
{
    public float DistanceBetweenPoints = 0.5f;

    private SpriteShapeController _shape;
    private Spline Spline => _shape.spline;
    [SerializeField] private List<SplinePoint> points = new List<SplinePoint>();

    private void Start()
    {
        Init();
        SaveOriginalPoints();
    }

    private void Init()
    {
        if (_shape == null) _shape = GetComponent<SpriteShapeController>();
    }

    [Button]
    public void SaveOriginalPoints()
    {
        Init();
        points.Clear();

        var pointCount = Spline.GetPointCount();
        for (var i = 0; i < pointCount; i++)
        {
            var center = Spline.GetPosition(i);
            var right = Spline.GetRightTangent(i);
            var left = Spline.GetLeftTangent(i);
            var mode = Spline.GetTangentMode(i);
            points.Add(new SplinePoint(center, left, right, mode));
        }
    }

    [Button]
    public void ResetToOriginalPoints()
    {
        Init();
        RemoveAllPoints();
        for (var i = 0; i < points.Count; i++)
        {
            Spline.InsertPointAt(i, points[i].Center);
            Spline.SetTangentMode(i, points[i].TangentMode);
            Spline.SetLeftTangent(i, points[i].Left);
            Spline.SetRightTangent(i, points[i].Right);
        }
    }

    // [Button]
    // public void AddResolution()
    // {
    //     List<Tuple<Vector2, int>> pointToInsert = new List<Tuple<Vector2, int>>();
    //
    //     var pointsCount = points.Count;
    //
    //     // Gather all the positions to be placed per spline node pair
    //     for (var i = 0; i < pointsCount; i++)
    //     {
    //         Vector2 a = Spline.GetPosition(i);
    //         Vector2 b = Spline.GetPosition((i + 1) % pointsCount);
    //
    //         Vector2 ab = b - a;
    //
    //         int amountToPlace = Mathf.FloorToInt(ab.magnitude / DistanceBetweenPoints);
    //         for (int j = 0; j < amountToPlace; j++)
    //         {
    //             Vector2 newPos = a + ab.normalized * DistanceBetweenPoints * (j + 1);
    //             pointToInsert.Add(new Tuple<Vector2, int>(newPos, i));
    //         }
    //     }
    //
    //     // Do a reverse count loop to add the points in between the spline pairs
    //     for (int i = pointsCount - 1; i >= 0; i--)
    //     {
    //         var toAdd = pointToInsert
    //             .Where(p => p.Item2 == i)
    //             .ToList();
    //         for (int j = 0; j < toAdd.Count; j++)
    //         {
    //             Spline.InsertPointAt(i + j, toAdd[j].Item1);
    //         }
    //     }
    // }

    [Button]
    public void AddResolution()
    {
        List<Tuple<Vector2, int>> pointToInsert = new List<Tuple<Vector2, int>>();

        var pointsCount = points.Count;

        int current = 0;
        bool firstime = true;
        while(current != 0 || firstime)
        {
            firstime = false;

            Vector2 a = Spline.GetPosition(current);
            var nextIndex = (current + 1) % points.Count;
            Vector2 b = Spline.GetPosition(nextIndex);

            Vector2 ab = b - a;

            int amountToPlace = Mathf.FloorToInt(ab.magnitude / DistanceBetweenPoints);
            for (int j = 0; j < amountToPlace; j++)
            {
                Vector2 newPos = a + ab.normalized * DistanceBetweenPoints * (j + 1);
                Spline.InsertPointAt(current + j, newPos);
            }

            current = nextIndex + amountToPlace;
        }
    }

    private void RemoveAllPoints()
    {
        Init();
        Spline.Clear();
    }

    [Serializable]
    public class SplinePoint
    {
        public Vector2 Center, Left, Right;
        public ShapeTangentMode TangentMode;

        public SplinePoint(Vector2 center, Vector2 left, Vector2 right, ShapeTangentMode tangentMode)
        {
            this.Center = center;
            this.Left = left;
            this.Right = right;
            TangentMode = tangentMode;
        }
    }
}