﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;
using UnityEngine.PlayerLoop;

[RequireComponent(typeof(CharacterMovementState))]
[RequireComponent(typeof(Rigidbody2D))]
public class BounceSlam : MonoBehaviour
{
    private CharacterMovementState state;
    private Rigidbody2D rb;
    private bool _previousGrounded = false;
    private float _lastNegativeY = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        state = GetComponent<CharacterMovementState>();
    }

    private void FixedUpdate()
    {
        if (rb.velocity.y < 0.0f && Mathf.Abs(rb.velocity.y) > 0.1f)
            _lastNegativeY = rb.velocity.y;

        if (state.Grounded && !_previousGrounded)
        {
            Events.Instance.Raise(new SlamJelly(rb.position, Mathf.Abs(_lastNegativeY)));
        }

        _previousGrounded = state.Grounded;
    }
}