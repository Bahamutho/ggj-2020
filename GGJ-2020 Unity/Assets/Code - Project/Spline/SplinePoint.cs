﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SplinePoint
{
    public Vector2 Anchor;
    public Vector2 Current;
    public int Index;
    public Vector2 Velocity;

    // TODO more cool variables

    public SplinePoint(Vector2 current, int index)
    {
        Anchor = current;
        Current = current;
        Velocity = Vector2.zero;
        Index = index;
    }

    public Vector2 LimitVelocity(float maxSpeed)
    {
        var vm = Velocity.magnitude;

        var speed = Mathf.Min(Mathf.Abs(vm), maxSpeed);
        speed = Mathf.Abs(speed) < 0.01f ? 0 : speed;

        return Mathf.Sign(vm) * speed * Velocity.normalized;
    }

    public void UpdatePosition(float timeStep, float maxSpeed)
    {
        Velocity = LimitVelocity(maxSpeed);
        Current += Velocity * timeStep;
    }

    public void UpdateVelocityWithSpring(float timeStep, float damping, float stiffness)
    {
        Vector2 dist = Current - Anchor;
        Vector2 unit = dist.normalized;

        float distance_error = dist.magnitude; //Vector2.Dot(unit, dist);
        float velocity_error = Vector2.Dot(unit, Velocity);

        float distance_impulse = stiffness * distance_error * 1 / timeStep;
        float velocity_impulse = damping * velocity_error;
        float impulse = -(distance_impulse + velocity_impulse) * damping;

        Velocity += impulse * unit;
    }
}
