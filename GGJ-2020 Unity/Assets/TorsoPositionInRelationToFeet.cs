﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

[ExecuteInEditMode]
public class TorsoPositionInRelationToFeet : MonoBehaviour
{
    public float HeightFromGround = 0.85f;

    public Transform Torso;
    private Vector2 torsoPos => Torso.position;

    void Start()
    {
        Events.Instance.AddListener<OnGrounded>(e => { UpdateBasedOnRayCast(); });
        Events.Instance.AddListener<OnAirborn>(e => { transform.position = torsoPos; });
    }

    private void UpdateBasedOnRayCast()
    {
        // Change this maybe to feet pos
        float x = torsoPos.x;

        Vector2 offset = Vector2.up * HeightFromGround;
        RaycastHit2D hit = Physics2D.Raycast(
            torsoPos + offset,
            Vector2.down,
            4 * HeightFromGround,
            layerMask: SettingsHolder.Instance.Settings.IkColliderLayerMask);
        if (hit.collider != null)
        {
            float y = hit.point.y + HeightFromGround;
            transform.position = new Vector2(x, y);
        }
    }
}