﻿using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class DisableMoveToTargetOnJump : MonoBehaviour
{
    private MoveRigidbody2DToTarget moveToTarget;
    public float GroundedTimeBeforeOn = 0.2f;
    public float AirbornTimeBeforeOff = 0.2f;

    void Start()
    {
        moveToTarget = GetComponent<MoveRigidbody2DToTarget>();
        Events.Instance.AddListener<OnGrounded>(e =>
        {
            if (e.state.GroundedTime > GroundedTimeBeforeOn && !moveToTarget.On)
            {
                moveToTarget.On = true;
                Debug.Log($"OnGrounded after {e.state.GroundedTime}");
            }
        });
        Events.Instance.AddListener<OnAirborn>(e =>
        {
            if (e.state.AirbornTime > AirbornTimeBeforeOff && moveToTarget.On)
            {
                moveToTarget.On = false;
                Debug.Log($"OnAirborn after {e.state.AirbornTime}");
            }
        });
        Events.Instance.AddListener<OnJumpStart>(e =>
        {
            moveToTarget.On = false;
            Debug.Log($"OnJumpStart");
        });
    }
}
